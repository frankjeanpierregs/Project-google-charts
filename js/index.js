
let $select = $('#years');
let $table = document.getElementById("table");
let datos;

document.getElementById('show').addEventListener('click', e => {

    //Inicializando variables necesarias
    let info = []; //Contendrá los valores que se graficarán
    let year = document.getElementById('years').value;
    let type = document.getElementById('typeS').selectedIndex;
    let option = document.getElementById('typeS').value;

    //Tipo de análisis
    type = type == 0 ? 1 : type == 1 ? 2 : type == 2 ? 3 : type == 3 ? 4 : 6;

    info.push([`${option}`, 'Sales']);

    //Inicializando tabla
    let table = crearTabla(option);

    [info, table] = analytics(datos[year], info, table, type);

    //Dibujando el chart
    draw(info);
    $('#div-bars').css({ 'display': 'inline' });
});

function crearTabla(option) {
    let table = `
            <table class="table table-dark">
            <thead>
            <tr>
            <th class="text-center" scope="col">${option}</th>
            <th class="text-center" scope="col">$ Sales</th>
            </thead>
        `;
    return table;
}

function draw(data) {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var info = google.visualization.arrayToDataTable(data);
        var config = {
            height: $('#table').innerHeight(),
            title: 'Sales $',
            colors: ['#28a745'],
            is3D: true,
            backgroundColor: {
                fill: '#fff'
            },
            bars: 'horizontal'
        };
        var chart = new google.charts.Bar(document.getElementById('barchart_material'));
        chart.draw(info, google.charts.Bar.convertOptions(config));
    }
}

function loadData() {
    let guardado = localStorage.getItem('datos');
    if (guardado) {
        //Cargando del localStorage
        datos = JSON.parse(guardado);
        initSelect();
    } else {
        //Cargando de /data
        const xhttp = new XMLHttpRequest();
        xhttp.open('GET', 'data/sales.json', true);
        xhttp.send();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                datos = JSON.parse(this.responseText);
                initSelect();
            }
        }
    }
}

function initSelect() {
    //Agregando al select los años guardados
    for (let year in datos) {
        $select.append(`<option> ${year} </option>`);
    }
}

function analytics(data, info, t, type) {
    var i = 1;
    var value = 0;
    var text = 1;
    for (month in data) {
        value += data[month];
        if (i % type == 0) {
            t += `
                <tr>
                <td>${text}</td>
                <td>${value}</td>
                </tr>
            `;
            info.push([text + "", value]);
            value = 0;
            text++;
        }
        i++;
    }
    t += `</table>`;
    $table.innerHTML = t;
    return [info, t];
}

window.onload = function () {
    this.loadData();
}